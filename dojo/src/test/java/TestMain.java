import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class TestMain {

    @Test
    public void test_isValid() {
        String result = Solution.isValid("");
        assertThat(result, equals(""));
    }
}
